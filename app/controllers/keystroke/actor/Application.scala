package controllers.keystroke.actor

import play.api._
import play.api.mvc._

import akka.actor.{Actor, Props}

import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application extends Controller {

  import Play.current

  def index = Action { implicit request =>
    Ok(views.html.keystroke.actor.index())
  }

  def charCounter = WebSocket.acceptWithActor[String, String] { request => out =>
    Props(new Actor {
      def receive = {
        case str: String => out ! str.length.toString
      }
    })
  }
}
