package controllers

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._

import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application extends Controller {

  def index = Action { implicit request =>
    Ok(views.html.index())
  }

  def charCounter = WebSocket.using[String] { request =>
    val (out, channel) = Concurrent.broadcast[String]
    val in = Iteratee.foreach[String] { inputString => {
      Thread.sleep(10000L)
      channel push (inputString.size.toString)
      }
    }

    (in, out)
  }
}