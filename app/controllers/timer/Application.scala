package controllers.timer

import play.api._
import play.api.mvc._
import akka.actor.{Actor, ActorRef, Props}
import scala.concurrent.duration._
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application extends Controller {

  import Play.current

  def index = Action { implicit request =>
    Ok(views.html.timer.index())
  }

  def timer = WebSocket.acceptWithActor[String, String] { request => out =>
    Timer.props(out)
  }
}

class Timer(out: ActorRef) extends Actor {

  val schedule = context.system.scheduler.schedule(0.second, 1.second, self, Timer.SendTime)

  def receive = {
    case Timer.SendTime =>
      val timeString = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"))
      out ! timeString
  }

  override def postStop {
    schedule.cancel
  }
}

object Timer {
  def props(out: ActorRef) = Props(new Timer(out))
  object SendTime
}
