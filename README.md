# play-websockets

A quick project demonstrating the use of the WebSockets protocol in the Play framework. The application calculates the
number of characters in a String, but as this is supposed to be a stand-in for a time-consuming task, it delays delivery
of the result of its calculation.
